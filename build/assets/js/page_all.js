/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2021. MIT licensed.
 */$(document).ready(function () {
  $('.navbar-toggler').click(function () {
    $('header nav').toggleClass('active');
  });
  $('.navbar-collapse .close,.modal-menu').click(function () {
    $('.navbar').removeClass('active');
  }); // active navbar of page current

  var urlcurrent = window.location.href;
  $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active');
  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
  });
  $('.navbar-nav > li').click(function () {
    $(this).toggleClass('active');
  }); //modal video

  $('.video').on('click', function () {
    let link_video = $(this).data('link');
    $('.monkey-modal-video iframe').attr('src', link_video);
  });
  $('.modal,.close').on('click', function () {
    let link_video = '';
    $('.monkey-modal-video iframe').attr('src', link_video);
  }); //show search

  $('.btn_search').on('click', function () {
    $('.box-search').addClass('active');
    $('.box-search input').focus();
  });
  $('.box-search .close').on('click', function () {
    $('.box-search').removeClass('active');
  }); //

  $(window).on('load', function () {
    var height_dr = $('footer .language .dropdown-menu').outerHeight() + 10;
    $('footer .language .dropdown-menu').css('top', -height_dr);
  });

  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight() - 100;
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height() - 100;
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  $(window).on('resize scroll load', function () {
    $('.fadeup').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeInUp').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.fadein').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeIn').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.zoomin').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('zoomIn').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
  });
});